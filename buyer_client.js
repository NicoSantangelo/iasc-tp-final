var Promise = require('bluebird')

var r       = require('./request_wrapper')
var urls    = require('./domain_urls')({
    baseUrl: process.env.URL,
    port   : process.env.PORT,
    cap    : process.env.PORT_CAP
})

var builder = require('./builder')

var KeyboardCommands = require('./keyboard-commands')

var buyer = {
    person: {},
    _register: function() {
        this.person = builder.person()
        console.log('Registrando a', this.person.name, 'a la subasta')

        var keyboard = new KeyboardCommands(buyer, this.person.name)

        r.post(urls.buyers, this.person).then(function(response) {
            console.log('Registro existoso!')
            buyer.person._id = response._id
            buyer._heartbeat()
            keyboard.listen()
        }).catch(function(err) {
            console.log('[ERROR] Error al registrar', err.message)
            if (r.isConnectionRefused(err)) {
                console.log('Reintentando...')
                setTimeout(function() {
                    urls.reconnect()
                    buyer._register()
                }, 700)
            }
        })
    },
    bid: function(title, price) {
        if (!title || !price) {
            console.log('No puede ofertar en una subasta sin los argumentos necesarios')
            console.log('bid title price')
            return
        }
        var params = {
            price: parseFloat(price),
            buyer: this.person
        }
        console.log('Publicando precio', price, 'en la subasta', title)

        r.put(urls.bids + '/' + title, params).then(function(response) {
            console.log('Precio publicado, respuesta', JSON.stringify(response, null, 0))
        }).catch(function(err) {
            console.log('[ERROR] Error al publicar', err.message)
        })
    },
    _heartbeat: function() {
        r.heartbeat(urls.heartbeat, 5000).catch(function() {
            urls.reconnect()
            buyer._heartbeat()
        }.bind(this))
    }
}

buyer._register()
