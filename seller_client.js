var Promise = require('bluebird')

var r       = require('./request_wrapper')
var urls    = require('./domain_urls')({
    baseUrl: process.env.URL,
    port   : process.env.PORT,
    cap    : process.env.PORT_CAP
})
var builder = require('./builder')

var KeyboardCommands = require('./keyboard-commands')

var seller = {
    person: {},
    _register: function() {
        this.person = builder.person()
        console.log('Registrando a', this.person.name)

        var keyboard = new KeyboardCommands(seller, this.person.name)

        r.post(urls.sellers, this.person).then(function(response) {
            console.log('Registro existoso!')
            seller.person._id = response._id
            seller._heartbeat()
            keyboard.listen()
        }).catch(function(err) {
            console.log('[ERROR] Error al registrar', err.message)
            if (r.isConnectionRefused(err)) {
                console.log('Reintentando...')
                setTimeout(function() {
                    urls.reconnect()
                    seller._register()
                }, 700)
            }
        })
    },
    // time in seconds
    bidding: function(title, price, time) {
        if (!title || !price || !time) {
            console.log('No puede crear una subasta sin los argumentos necesarios')
            console.log('bidding title price time_segundos')
            return
        }
        var params = {
            title : title,
            price : parseFloat(price),
            time  : parseInt(time, 10),
            seller: this.person,
        }

        r.post(urls.bids, params).then(function(response) {
            console.log('Subasta existosa, respuesta', JSON.stringify(response, null, 0))
        }).catch(function(err) {
            console.log('[ERROR] Error al crear subasta', err.message)
        })
    },
    cancel: function(title) {
        if (!title) {
            console.log('No puede cancelar una subasta sin el título')
            return
        }

        r.delete(urls.bids + '/' + title).then(function(response) {
            console.log('Subasta cancelada, respuesta', response)
        }).catch(function(err) {
            console.log('[ERROR] Error al crear subasta', err.message)
        })
    },
    _heartbeat: function() {
        r.heartbeat(urls.heartbeat, 5000).catch(function() {
            urls.reconnect()
            seller._heartbeat()
        }.bind(this))
    }
}

seller._register()