var actionNames = ['buyers', 'sellers', 'bids', 'heartbeat']

module.exports = function(options) {
    var opts = options || {}
    var baseUrl = opts.baseUrl || 'http://127.0.0.1' 
    var port    = +opts.port   || (opts.testing ? 8888 : 3000)
    var cap     = opts.cap     || 10
    var portCap = port + cap

    var defaultData = {
        port: port,
        base: baseUrl + ':' + port,
        actions: {},
        reconnect: function() {
            var oldBaseUrl = this.base

            this.port = this.port + 1

            if (this.port > portCap) {
                this.port = port
            }

            this.base = baseUrl + ':' + this.port

            if (!opts.testing) {
                console.log('El servidor que se encontraba en', oldBaseUrl, 'parece estar caído, pasando a', this.base)
            }

            setActionsTo(this)

            return this.port
        }
    }

    var setActionsTo = function(object) {
        return actionNames.reduce(function(memo, actionName) {
            var action = '/' + actionName
            memo[actionName] = defaultData.base + action
            memo.actions[actionName] = action
            return memo
        }, object)
    }

    return setActionsTo(defaultData)
}