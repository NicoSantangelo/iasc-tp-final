var r = require('./request_wrapper')

var INTERVALID
var SERVERID
var supervisorUrl = process.env.SUPERVISOR_URL || 'http://127.0.0.1:4000'
var failedForwards = []

var SupervisorClient = function(url, fallback) {
    this.url = url

    // Should return the intervalId of the fallback version
    this.fallback = fallback

    this.boundCatch = this.catchConnectionError.bind(this)
}

SupervisorClient.prototype = {
    isOnline: function() {
        return !!SERVERID
    },

    register: function() {
        return r.post(supervisorUrl + '/register', { url: this.url }).then(function(response) {
            console.log('Conectado correctamente con el supervisor, el ID de este servidor es', response.serverId)
            SERVERID = response.serverId
            clearInterval(INTERVALID)
            if (failedForwards.length) {
                failedForwards.forEach(function(func) { func() })
                failedForwards = []
            }
        }).catch(this.boundCatch)
    },

    forward: function(bidding) {
        return r.post(supervisorUrl + '/bid', {
            serverId: SERVERID,
            biddingId: bidding._id
        }).catch(function() {
            failedForwards.push(this.forward.bind(this, bidding))
            this.catchConnectionError()
        }.bind(this))
    },

    delete: function(bidding) {
        if (this.isOnline()) {
            r.delete(supervisorUrl + '/bid', {
                serverId: SERVERID,
                biddingId: bidding._id
            }).catch(this.boundCatch)
        }
    },

    catchConnectionError: function() {
        SERVERID = undefined

        clearInterval(INTERVALID)
        INTERVALID = this.fallback()

        setTimeout(function() {
            this.register()
        }.bind(this), 10000)
    },

}

module.exports = SupervisorClient
