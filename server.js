////
// MODELS
////

var mongoose = require('mongoose')
var Promise = require('bluebird')

mongoose.Promise = Promise
mongoose.connect(process.env.TEST ? 'mongodb://localhost/iac_test' : 'mongodb://localhost/iac')

var Buyer = require('./models/buyer')
var Seller = require('./models/seller')
var Bidding = require('./models/bidding')
var BidPrice = require('./models/bidPrice')


////
// SERVER
////

var express = require('express')
var app = express()
var bodyParser = require('body-parser')

var urls = require('./domain_urls')({
  testing: process.env.TEST,
  port: process.env.PORT,
  cap: process.env.PORT_CAP
})

var SupervisorClient = require('./supervisor_client')

app.use(bodyParser.json())


//
// API
//

//
// Buyers
app.route(urls.actions.buyers)
  .post(function (req, res) {
    console.log('Nuevo comprador', req.body)
    var buyer = new Buyer(req.body)

    buyer.save().then(function () {
      res.json(buyer)
    })
  })

//
// Sellers
app.route(urls.actions.sellers)
  .post(function (req, res) {
    console.log('Nuevo vendedor', req.body)
    var seller = new Seller(req.body)

    seller.save().then(function () {
      res.json(seller)
    })
  })

//
// Bids

app.route(urls.actions.bids)
  .post(function (req, res) {
    console.log('Nueva subasta', req.body)
    var attrs = req.body
    var time = attrs.time

    var endTime = new Date()
    endTime.setSeconds(endTime.getSeconds() + attrs.time)

    attrs.endTime = endTime
    attrs.time = time * 1000
    attrs.status = 'Open'

    new Bidding(attrs).save().then(function (newBidding) {
      if (supervisorClient.isOnline()) {
        supervisorClient.forward(newBidding)
        closeOnTimeout(newBidding)
      }

      // Notify new bidding
      Buyer.find().then(function (buyers) {
        var notifyNewBidding = notify.bind(null,
          'Se ha creado una nueva subasta ' + newBidding.title + ' con un precio base de ' +
          newBidding.price + '. Expira en ' + time + ' segundos [' + endTime + ']'
          )
        buyers.forEach(notifyNewBidding)
      })

      return newBidding
    }).then(function (newBidding) {
      res.json(newBidding)
    })
  })

app.route(urls.actions.bids + '/:title')
  .put(function (req, res) {
    Bidding.oneByTitle(req.params.title).then(function (bidding) {
      if (!bidding) {
        throw 'No se encontró una subasta donde agregar el precio para "' + req.params.title + '"'
      }

      console.log('Se recibió el precio', req.body.price, 'para la subasta actual de precio', bidding.price)

      var newBid = new BidPrice({ price: req.body.price, bidding: bidding, buyer: req.body.buyer })

      newBid.save().then(function (savedBid) {
        return BidPrice.findMaxPriceById(bidding._id)
      }).then(function (winnerBid) {
        if (newBid.price >= winnerBid.price) {
          return bidding.update({
            price: newBid.price,
            buyer: newBid.buyer
          })
        } else {
          throw new Error('Halt chain, the price is not big enough')
        }
      }).then(function (updateResult) {
        notify('El precio ' + newBid.price + ' fue aceptado para "' + bidding.title + '"', newBid.buyer)
        return Buyer.allExcept(newBid.buyer)
      }).then(function (buyers) {
        var newPriceNotify = notify.bind(null, 'La subasta "' + bidding.title + '" tiene un nuevo precio: ' + newBid.price)
        buyers.forEach(newPriceNotify)
      }).catch(function() {
        // Nothing to do here...
      }).finally(function() {
        res.json(bidding)
      })

    }).catch(function (message) {
      console.log('[ERROR] PUT /bids/:title =>', message)
      res.json({ message: 'Not found' })
    })
  })

  .delete(function (req, res) {
    Bidding.oneByTitle(req.params.title).then(function (bidding) {
      if (!bidding) {
        throw 'No se encontró una subasta a cancelar para "' + req.params.title + '"'
      }

      console.log('Cancelando la subasta "', bidding.title, '"')

      bidding.cancel().then(function () {
        supervisorClient.delete(bidding)

        return BidPrice.closeBids(bidding._id).then(function () {
          return Buyer.find()
        }).then(function (buyers) {
          var notifyCancelledBidding = notify.bind(null, 'La subasta ' + bidding.title + ' ha sido cancelada')
          buyers.forEach(notifyCancelledBidding)
          res.json({ message: 'Cancelled' })
        })
      })
    }).catch(function (message) {
      console.log('[ERROR] DELETE /bids/:title =>', message)
      res.json({ message: 'Not found' })
    })
  })

//
// Heartbeat

app.route(urls.actions.heartbeat)
  .get(function(req, res) {
    res.json({})
  })

//
// Supervisor

app.route('/handle_server_offline')
  .post(function(req, res) {
    var biddingIds = req.body.biddingIds || []

    console.log("Llegaron los biddings activos de otro servidor caído = ", biddingIds)

    biddingIds.forEach(function(biddingIds) {
      Bidding.findOne({ _id: biddingIds }).then(closeOnTimeout)
    })

    res.json({ status: 'ok' })
  })

//
// Utils
//


var closeOnTimeout = function(bidding) {
  if (!bidding) {
    return
  }

  setTimeout(function() {
    Bidding.findOne({ _id: bidding._id }).then(closeBidding)
  }, bidding.time)
}

var closeBidding = function(bidding) {
  if (bidding.status !== 'Open') {
    supervisorClient.delete(bidding)
    return
  }
  bidding.close().then(notifyClosing)
}

var monitorBiddings = function() {
  return setInterval(function() {
    Bidding.closeFinished().then(function (closedBiddings) {
      closedBiddings.forEach(notifyClosing)
    })
  }, 5000)
}

var notifyClosing = function(closedBidding) {
  notify('La subasta "' + closedBidding.title + '" terminó a ' + closedBidding.price, closedBidding.seller)

  if (closedBidding.buyer) {
    notify('Felicitaciones! Haz ganado la subasta "' + closedBidding.title + '" !!', closedBidding.buyer)

    closedBidding.findLosers().then(function(losers) {
      var notifyLoser = notify.bind(null, 'Lamentablemente ' + closedBidding.title + ' finalizó y no te ha sido adjudicada')
      losers.forEach(notifyLoser)
    })
  }

  supervisorClient.delete(closedBidding)
}

var notify = function (message, person) {
  // [send email | log] to person.contact
  console.log('[NOTIFICATION] Sending "', message, '" to', person.contact)
}


//
// Start the server
//

var listen = function() {
  var server = app.listen(urls.port, function() {
    var suffix = 'Empezando servidor en el puerto ' + server.address().port

    supervisorClient = new SupervisorClient(urls.base, monitorBiddings)

    if (process.env.TEST) {
      console.log('[TEST] ' + suffix)
    } else {
      console.log(suffix)
      supervisorClient.register().finally(function() {
        if (!supervisorClient.isOnline()) {
          console.log('Trabajando sin supervisor por ahora...')
        }
      })
    }
  })
  return server
}

listen().on('error', function(err) {
  urls.reconnect()
  listen()
})
