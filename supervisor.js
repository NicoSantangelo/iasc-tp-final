////
// MODELS
////

var mongoose = require('mongoose')
var Promise = require('bluebird')

mongoose.Promise = Promise
mongoose.connect(process.env.TEST ? 'mongodb://localhost/iac_test' : 'mongodb://localhost/iac')

var Bidding = require('./models/bidding')

////
// SERVER
////

var express = require('express')
var app = express()
var bodyParser = require('body-parser')

var r    = require('./request_wrapper')
var urls = require('./domain_urls')({ port: process.env.PORT || 4000 })

app.use(bodyParser.json())

var serverCounter = 1
var activeServers = {}

var sendBiddingsToTheNextServer = function(id, offlineServer) {
    offlineServer = offlineServer || activeServers[id]
    delete activeServers[id]

    console.log('Se detectó la caída de ', offlineServer.url)

    if (!Object.keys(offlineServer.biddings).length) {
        console.log(offlineServer.url, 'no tenía biddings activos. No se realiza ninguna acción')
        return
    }

    var retry = function() {
        setTimeout(function() {
            sendBiddingsToTheNextServer(id, offlineServer)
        }, 10000)
    }
    var serverIds = Object.keys(activeServers)
    if (serverIds.length) {
        var activeServer = activeServers[serverIds[0]]
        var biddingIds = Object.keys(offlineServer.biddings).map(function (key) {
            return offlineServer.biddings[key]
        })

        console.log('Enviando ', biddingIds, 'a ', activeServer.url, 'a partir de la caída de', offlineServer.url)

        r.post(activeServer.url + '/handle_server_offline', {
            biddingIds: biddingIds
        }).then(function() {
            Object.keys(offlineServer.biddings).forEach(function(key) {
                activeServer.biddings[key] = offlineServer.biddings[key]
            })
            console.log('Envío a', activeServer.url, 'correcto. Servers activos =', activeServers)
        }).catch(function(response) {
            console.log('Ocurrió un error contactando', activeServer.url, 'reintentando en 10 segundos.')
            retry()
        })
    } else {
        console.log('No hay servidor activo donde mandar las biddings de ', offlineServer.url, ' reintenantdo en 10 segundos')
        return retry()
    }
}

app.route('/register')
  .post(function(req, res) {
    var id = serverCounter++
    var url = req.body.url

    console.log('Registrando Servidor ', id, 'de url', url)
    
    // TODO: Check if id exists and retry to avoid stepping over servers
    activeServers[id] = {
        url: url,
        biddings: {}
    }

    r.heartbeat(url + '/heartbeat', 2000).catch(function() {
        // Add 5 seconds to each bidding
        var biddings = activeServers[id].biddings
        var promises = Object.keys(biddings).map(function (key) {
            return Bidding.update({ _id: biddings[key] }, { $inc: { time: 5000 } })
        })

        Promise.all(promises).then(function() {
            sendBiddingsToTheNextServer(id)
        })
    })

    res.json({ serverId: id })
  })

app.route('/bid')
  .post(function(req, res) {
    var serverId  = req.body.serverId
    var biddingId = req.body.biddingId

    console.log('[POST] Llego el bidding', biddingId, 'del servidor', serverId)

    if (activeServers[serverId]) {
        activeServers[serverId].biddings[biddingId] = biddingId
        console.log('[POST] Bidding id registrado. Servers activos =', activeServers)
    } else {
        console.log('[POST] Servidor no encontrado entre los servidores activos')
    }

    res.json({ status: 'ok' })
  })
  .delete(function(req, res) {
    var serverId  = req.body.serverId
    var biddingId = req.body.biddingId

    console.log('[DELETE] Se cerró el bidding', biddingId, 'del servidor', serverId)

    if (activeServers[serverId]) {
        delete activeServers[serverId].biddings[biddingId]
        console.log('[DELETE] Bidding id registrado. Servers activos =', activeServers)
    } else {
        console.log('[DELETE] Servidor no encontrado entre los servidores activos')
    }

    res.json({ status: 'ok' })
  })



var server = app.listen(urls.port, function() {
    console.log('Empezando supervisor en el puerto ' + server.address().port)
})
