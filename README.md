# Indice

* [Tecnología](#tecnología)
* [Arquitectura general](#arquitectura-general)
* [Configuracion](#configuracion)
* [Correr el proyecto](#correr-el-proyecto)

# Teconología

Para el desarrollo del TP, utilizamos [NodeJS](https://nodejs.org/en/) para el desarrollo del servidor y los clientes y [MongoDB](https://www.mongodb.org/) para la persistencia.

Además, se hace uso intensivo de dos librerías:

* [Mongoose](http://mongoosejs.com/) para abstraer la comunicación con la base de datos y proveer Schemas
* [Bluebird](http://bluebirdjs.com): Para el manejo de promesas

y, por último, se utilizó [Mocha](https://mochajs.org) con [Chia](http://chaijs.com/) para los tests.

# Arquitectura general

Debido a que NodeJS no nos provee con una solución para implementar supervisión, debimos encontrar una forma de manejar la caida de un servidor por parte de los `clientes` y del `server`

### Modelos

Utlizamos 4 modelos (que tienen una representación directa en la Base de Datos)

* `Buyer`: Cada comprador registrado
* `Seller`: Cada vendedor registrado
* `Biddings`: Cada subasta. Tiene un estado (`Creada/Cancelada/Cerrada`) y guarda el tiempo de finalización
* `BidPrice`: Encargado de reprsentar cada precio que le llega de un comprador para una subasta en particula``r

### Clientes

Cada cliente (comprador/vendedor) implementa un heartbeat con el server cada 5 segundos. Si alguno de los beats no llega a destino (retorna `Connection refused`) pasará a intentar conectarse al siguiente puerto de la URL base actual, hasta un cierto límite, donde volverá a empezar por el puerto de comienzo.

El límite por defecto es de `10` puertos, pero puede ser [configurado por variables de entorno](#configuracion).

Por ejemplo:

* Comprador empieza en `http://localhost:3000`
* El server se cae
* Comprador intenta conectarse a `http://localhost:3001`, 
    * De ser exitoso, mantiene la conexión
    * De fallar, pasa al próximo puerto (`3002`). De llegar a `3010`, comienza nuevamente desde `3000`


### Servidor

El inconveniente con el que nos encontramos al momento de implementar el servidor, es que al no tener comunicación `built-in`, armar una propia para no perder información y estado entre ellos no es una tarea trivial.

Debido a esto, tomamos dos alternativas

#### Orientado a la base de datos

Decidimos utilizar la Base de Datos como única fuente de verdad y como estado compartido entre servidores.
Esto trae algunas desventajas (como que se debe atacar la base cada cierto tiempo fijo, análogo a hacer realtime desde un Browser sin AJAX), pero tiene la ventaja de hacer que la comunicación intra-servidor sea nula.

La forma en la funciona cada servidor es la siguiente:

* Por cada nuevo precio para una subasta, sin hacer preguntas, guardarlo como `BidPrice`
* Cada X tiempo, buscar subastas que deben ser cerradas. 
* Por cada subasta
    * Intentar cerrarla. Esto significa, tomar todos sus `BidPrices` y realizar un update en ellos, agregando un timestamp (señalizando la finalización de la subasta). El punto importante es que un `update` en Mongo requiere una query y que puede ser aislado usando [$isolated](https://docs.mongodb.org/manual/reference/operator/update/isolated/#up._S_isolated).
    Entonces, el update busca `update_isolated({ bid id = X && timestamp !exists }, { set: timestamp = now })`.
        * Si esa consulta pudo escribir quiere decir que el server puede continuar cerrando la subasta, seteando el estado indicado en `Bidding` y notificando.
        * Si no pudo escribir otro server llegó primero e ignoramos la subasta.

La desventaja principal es que `$isolated` no soporta datos `shardeados`, con lo cual al momento de distribuir nuestra base de mongo tenemos que tener mucho cuidado qué key usamos para dividir los datos así como también, la estrategia que usaremos de querer replicar.

#### Supervisor

Escribimos un supervisor `'manual'` que tiene como tareas mantener una lista de las subastas activas actualmente para cada servidor y de, ante la caída de un servidor detectada mediante un heartbeat, fordwardear las subastas hacia otro servidor activo.

Si el supervisor no existe, los servidores vuelven al modo [base de datos](#base-de-datos) y reintentan conectarse luego de un tiempo.

Funcionamiento:

* El supervisor está a la espera de nuevos servidores
* Cuando uno se registra con un `id` único y setea un `heartbeat` para determinar su estado
* Mientras esté vivo
    * Escucha por nuevos biddings y cuando estos se cierran y mantiene una lista actualizada (por server id) de ambas
* Al morir
    * Busca en su lista de servidores activos a uno donde pueda enviar los biddings que el servidor caído no pudo manejar
    * De no encontrar ninguno, retrasa la acción un tiempo hasta poderla satisfacer

Esta implementación corre con la ventaja de que la base de datos ya no es un cuello de botella.
Tiene como inconvenientes que cada server debe soportar un `heartbeat` continuo tanto de los clientes como del supervisor y que al ser una implementación casera, puede tener inconvenientes con casos límite.

En este momento solo se soporta un solo supervisor

Para configurar la comunicación entre supervisor y servers mirar la sección [configuración](#configuracion)

**Nota**: La diferencia principal entre este supervisor y uno de los encontrados en otros leguajes que los soportan es que los servidores son procesos independientes. Es decir, ante la caída de uno de ellos y si no hay otro corriendo, es responsabilidad del administrador levantar otro.

# Configuracion

Los clientes y el servidor pueden ser configurados mediante variables de entorno, por ejemplo `TEST=true node server`.

Su uso se describe a continuación.

### Clientes

* `URL`: URL base donde conectarse, `http://127.0.0.1` por defecto
* `PORT`: Puerto del server remoto. `3000` por defecto.
* `PORT_CAP`: Cantidad de puertos máxima donde intentar bindear. `10` por defecto.

### Server

* `PORT`: Puerto en el que bindear el server. Si el puerto se encuentra en uso, utiliza la misma lógica descrita para la [conexión de los clientes](#clientes) e intenta reconectar en el próximo. `3000` por defecto.
* `PORT_CAP`: Cantidad de puertos máxima donde intentar bindear. `10` por defecto.
* `SUPERVISOR_URL`: URL usada para conectarse al supervisor. Por defecto `http://127.0.0.1:4000`.
* `TEST`: Corre el server como soporte para tests (usa otra base de datos). Necesario para correr los tests del server de mocha.

### Supervisor

* `PORT`: Puerto en el que bindear el supervisor. Por defecto `4000`

# Correr el proyecto

*Instalación*

```bash
# Install mongodb
mongod --fork --logpath /var/log/mongod.log
npm install
# TEST=true node server
mocha /spec/*
```

*Server*

```bash
node server.js

# Si se quiere tener resiliencia (sin tener que empezar un nuevo servidor a mano) correr:
# PORT=3001 node server.js
# PORT=3002 node server.js
```

*Clientes*

```bash
node buyer_client.js
```

```bash
node seller_client.js
```

*Supervisor (opcional)*

```bash
node supervisor.js
```
