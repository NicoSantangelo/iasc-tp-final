var KeyboardCommands = function(commandMap, prefix) {
    this.commands = commandMap
    this.prefix = prefix
}


KeyboardCommands.prototype = {
    listen: function() {
        var commands = Object.keys(this.commands).filter(function(command) {
            return command[0] !== '_' && typeof(this.commands[command]) === 'function'
        }, this)

        process.stdin.resume()
        process.stdin.setEncoding("utf8")

        console.log("\nEscuchando comandos...(quit para salir)")
        console.log("Comandos disponibles: ", commands, 'ejecutarlos para ver los argumentos disponibles. Un argumento por espacio')

        this.printPrefix()
        process.stdin.on("data", this.onStdinData.bind(this, commands))
    },
    onStdinData: function (commands, text) {
        // TODO: Support command line like arguments (between quotes)
        var fullCommand = text.trim().split(" ")
        var command = fullCommand[0]
        var method  = this.commands[command]

        if (command === "quit") {
            console.log("Arrivederci")
            return process.exit()
        }

        if (method && commands.indexOf(command) !== -1) {
            method.apply(this.commands, fullCommand.slice(1))
        } else {
            if (command) {
                console.log("Eh?")
            }
        }
        this.printPrefix()
    },
    printPrefix: function() {
        if (this.prefix) {
            process.stdout.write(this.prefix + "> ")
        }
    }
}


module.exports = KeyboardCommands