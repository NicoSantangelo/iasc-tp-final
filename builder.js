var builder = {
    names: ["Pedro", "Carlos", "Marta", "Rosana", "Florencia", "Esteban", "Lenoardo", "Carolina",
            "Nicolas", "Cristian", "Axel", "Franco", "Pedro", "Martin", "Ezequiel", "Federico"],
    person: function() {
        var index = Math.floor(Math.random() * this.names.length)
        var name = this.names[index]
        return {
            name: name,
            contact: name.toLowerCase() + "@gmail.com"
        }
    }
}


module.exports = builder