var mongoose = require('mongoose')
var Promise = require('bluebird')

mongoose.Promise = Promise

var buyerSchema = mongoose.Schema({
  name: String,
  contact: String
})

buyerSchema.statics = {
  allExcept: function(buyer, properties) {
    return buyer ? Buyer.find({ _id: { $ne: buyer._id } }, properties) : new Promise(function() {})
  }
}

var Buyer = mongoose.model('Buyer', buyerSchema)

module.exports = Buyer