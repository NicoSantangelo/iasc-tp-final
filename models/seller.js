var mongoose = require('mongoose')
var Promise = require('bluebird')

mongoose.Promise = Promise

var Seller = mongoose.model('Seller', {
    name: String,
    contact: String
})

module.exports = Seller