var mongoose = require('mongoose')
var Mixed = mongoose.Schema.Types.Mixed
var Buyer = require('./buyer')
var BidPrice = require('./bidPrice')

var Promise = require('bluebird')

mongoose.Promise = Promise

var bidSchema = mongoose.Schema({
  title: String,
  price: Number,

  endTime: Date,
  time: Number,

  status: String, // Open | Closed | Cancelled

  seller: Mixed,
  buyer: Mixed,
})

//
// Instance methods
//

bidSchema.methods = {
  close: function () {
    var self = this
    console.log('Cerrando la subasta de ID', this._id, 'después de', this.time / 1000, 'segundos')
    return new Promise(function (resolve, reject) {
      BidPrice.closeBids(self._id).then(function () {
        BidPrice.findMaxPriceById(self._id).then(function (winnerBid) {
          return self.update({
            price: winnerBid.price || self.price,
            buyer: winnerBid.buyer,
            status: 'Closed'
          }).then(function (updateResult) {
            resolve(self)
          })
        })
        return self
      }).catch(function (message) {
        reject(message)
      })
    })
  },
  cancel: function() {
    this.status = 'Cancelled'
    return this.save()
  },
  findLosers: function(props) {
    return Buyer.allExcept(this.buyer, props)
  }
}


//
// Class methods
//

bidSchema.statics = {
  oneByTitle: function(title, props) {
    return Bidding.findOne({
      title: title,
      status: 'Open'
    }, props)
  },
  
  findOpened: function () {
    var now = new Date()
    now.setSeconds(now.getSeconds() + 5)
    
    return Bidding.find({
      status: 'Open',
      endTime: { $lte: now }
    });
  },
  
  closeFinished: function () {
    return new Promise(function (resolve, reject) {
      Bidding.findOpened().then(function (openedBiddings) {
        return Promise.each(openedBiddings, function (openBid) {
          openBid.close().then(function () {
            resolve(openedBiddings);
          }).catch(function (message) {
            reject(message)
          });
          return openedBiddings
        })
      })
    })
  },
}

var Bidding = mongoose.model('Bidding', bidSchema)

module.exports = Bidding