var mongoose = require('mongoose')
var Promise = require('bluebird')
var Mixed = mongoose.Schema.Types.Mixed

mongoose.Promise = Promise

var bidPriceSchema = mongoose.Schema({
    timestamp: Date,
    price: Number,
    bidding: Mixed,
    buyer: Mixed
})

bidPriceSchema.statics = {
    findMaxPriceById: function (biddingId) {
        return BidPrice.findMaxPrice({ 'bidding._id': biddingId })
    },
    findMaxPrice: function(query) {
        return BidPrice.findOne({
            $query: query,
            $orderby: { price: -1 }
        }).then(function(winnerBid) {
            return winnerBid || {}
        })
    },
    closeBids: function (id) {
        return new Promise(function (resolve, reject) {
            BidPrice.tryToClose(id).then(function (updateResult) {
                if (updateResult.ok) {
                    resolve()
                } else {
                    reject('Looks like another server handled this bid quicker than us')
                }
            }).catch(function (message) {
                reject(message)
            })
        })
    },
    tryToClose: function (biddingId) {
        var query = { 
            'bidding._id': biddingId,
            timestamp: { $exists: false },
            $isolated : 1
        }
        var set = {
            $set: { timestamp: Date() }
        }
        return BidPrice.update(query, set, { multi: true })
    }

}

var BidPrice = mongoose.model('BidPrice', bidPriceSchema)

module.exports = BidPrice