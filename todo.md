# TODO

### POST /buyers

*OK*

### POST /sellers

*OK*

### POST /bids

Se debe manejar la concurrencia en caso de tener más de un server

* Hoy en día
    * LLegan los atributos de un nuevo Bidding.
    * Se calcula el tiempo de terminación
    * Se guarda el Bidding en la base. De guardarse bien:
        * Se notifican a todos los Buyers persistidos en el sistema
        * Se planifica el Bidding para cerrarlo (`scheduleClosing`). Cuando se cierra (status `Closed`)
            * Se notifica al vendedor
            * Se notifica al ganador (si existe)
            * Se notifica a los perdedores

* TODO:
    * LLegan los atributos de un nuevo Bidding.
    * Se calcula el tiempo de terminación
    * Se guarda el Bidding en la base. De guardarse bien:
        * Se notifican a todos los Buyers persistidos en el sistema
    * Cada X tiempo
        * Se verifica en la BDD si alguna Bidding debería cerrarse. De haber una cerrada:
            * Updatear `Bids` (o `BidPrices`) seteando un `timestamp` y haciendo un update con una query: `db.bids_price.update({ bid_id: ID, timestamp: { $exists: false } }, { $set: { timestamp: now } }, { multi: true })`
            * Si el update fue existoso, cerrar el subasta (`bidding`) sino no hacer nada ( lo tomó otro antes ) como antes, notificando a todos los indicados (usar `max()` para saber quíen tiene el precio máximo)


### PUT /bids/:title

Se debe manejar la concurrencia en caso de tener más de un server

* Hoy en día
    * LLegan los atributos de un nuevo Bid (alguien quiere poner un precio)
    * Se busca por título una subasta (`bidding`) no cerrada. De encontrarse:
        * Se fija que el precio actual sea menor al que llega en el request. De ser así
            * Lo setea en la base y avisa al comprador y al resto de los compradores
        * Si no es así
            * No hace nada

* TODO
    * LLegan los atributos de un nuevo Bid (alguien quiere poner un precio)
    * Se busca por título una subasta no cerrada. De encontrarse:
        * Guardar directamente en la colección `Bids` (o `BidPrices`) el precio, con el `bid_id`, buyer y **SIN** `timestamp` (que represnta que la subasta se cerró)
        * Traer el `max()` de esa colección, si el precio es mayor:
            * Avisa al comprador y al resto de los compradores


### DELETE /bids/:title


Se debe agregar el uso de la colección de precios.

* Una vez que se cancela una subasta (`bidding`)
    * Setear `timestamp` en `Bids` (o `BidPrices`).
