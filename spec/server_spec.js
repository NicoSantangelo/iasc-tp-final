var chai = require("chai")
var expect = chai.expect

var Promise = require('bluebird')

var r      = require('../request_wrapper')
var urls   = require('../domain_urls')({ testing: true })

var Buyer    = require('../models/buyer')
var Bidding  = require('../models/bidding')
var BidPrice = require('../models/bidPrice')

var helper = require('./spec_helper')


describe('express REST API server', function(){
    var biddingTitle = 'Ganate una motoneta'

    var buyer = {
        name: 'Nicolas',
        contact: 'nicolas@gmail.com'
    }

    var seller = {
        name: 'Vendedor',
        contact: 'vendedor@gmail.com'
    }

    var findBidding = function() {
        return Bidding.findOne({ title: biddingTitle })
    }
    var saveBidding = function(status) {
       return new Bidding({ title: biddingTitle, status: status || 'Open' }).save()
    }

    before(function() {
        helper.createBuyers()
    })

    describe('Register (POST) a Buyer', function() {it('should return the newly created Buyer', function(){
            return r.post(urls.buyers, buyer).then(function(savedBuyer) {
                expect(savedBuyer._id).to.not.be.undefined
                expect(savedBuyer.name).to.equal('Nicolas')
                expect(savedBuyer.contact).to.equal('nicolas@gmail.com')
            })
        })
    })

    describe('Register (POST) a Seller', function() {it('should return the newly created Seller', function(){
            return r.post(urls.sellers, seller).then(function(savedSeller) {
                expect(savedSeller._id).to.not.be.undefined
                expect(savedSeller.name).to.equal('Vendedor')
                expect(savedSeller.contact).to.equal('vendedor@gmail.com')
            })
        })    
    })

    describe('Create (POST) a Bidding', function() {
        it('should create a new bidding with the appropiate time and status', function(){
            var bidding = {
                title : 'TestBid',
                price : 42,
                time  : 3600,
                seller: seller
            }

            return r.post(urls.bids, bidding).then(function(savedBidding) {
                bidding.time = 3600000

                expect(savedBidding._id).to.not.be.undefined
                expect(new Date(savedBidding.endTime)).to.not.equal('Invalid Date')
                expect(savedBidding).to.contain.all.keys(bidding)
            })
        })
    })

    describe('Add a price (PUT) to a Bidding', function() {
        it('should return "Not found" if the bidding does not exist', function(){
            return r.put(urls.bids + '/' + biddingTitle).then(function(response) {
                expect(response).to.deep.equal({ message: 'Not found' })
            })
        })

        it('should return "Not found" if the bidding is not open', function(){
            return saveBidding('Closed').then(function() {
                return r.put(urls.bids + '/' + biddingTitle).then(function(response) {
                    expect(response).to.deep.equal({ message: 'Not found' })
                })
            })
        })

        it('should create a new BidPrice without a timestamp and set the price and winner on the bidding for future use if the price is greater than max', function(){
            return saveBidding('Open').then(function() {
                return r.put(urls.bids + '/' + biddingTitle, { price: 20, buyer: buyer }).then(function(response) {
                    return Promise.all([
                        findBidding(),
                        BidPrice.find({ 'bidding.title': biddingTitle })
                    ]).spread(function(bidding, bidPrices) {
                        expect(bidPrices.length).to.equal(1)

                        expect(bidding.price).to.equal(20)
                        expect(bidding.buyer).to.deep.equal(buyer)

                        var bidPrice = bidPrices[0]
                        expect(bidPrice.price).to.equal(20)
                        expect(bidPrice.timestamp).to.be.undefined
                        expect(bidPrice.buyer).to.deep.equal(buyer)
                    })
                })
            })
        })

        afterEach(function() {
            return helper.cleanDB()
        })
    })

    describe('Cancel (DELETE) a Bidding', function() {
        it('should return "Not found" if the bidding does not exist', function(){
            return r.delete(urls.bids + '/' + biddingTitle).then(function(response) {
                expect(response).to.deep.equal({ message: 'Not found' })
            })
        })

        it('should return "Not found" if the bidding is not open', function(){
            return saveBidding('Closed').then(function() {
                return r.delete(urls.bids + '/' + biddingTitle).then(function(response) {
                    expect(response).to.deep.equal({ message: 'Not found' })
                })
            })
        })

        it('should set the Bid status to Cancelled and the timestamp on the BidPrices', function(){
            return saveBidding('Open').then(function(newBidding) {
                return Promise.all([
                    new BidPrice({ bidding: newBidding, price: 42 }).save(),
                    new BidPrice({ bidding: newBidding, price: 43 }).save(),
                    new BidPrice({ bidding: newBidding, price: 44 }).save()
                ]).then(function() {
                    return r.delete(urls.bids + '/' + biddingTitle).then(function() {
                        Promise.all([
                            findBidding(),
                            BidPrice.count({ 'bidding.title': biddingTitle, timestamp: { $exists: false } })
                        ]).spread(function(bidding, bidPriceCount) {
                            expect(bidding.status).to.equal('Cancelled')
                            expect(bidPriceCount).to.equal(0)
                        })
                    })
                })
            })
        })

        beforeEach(function() {
            return helper.cleanDB()
        })
    })

    describe('Heartbeat (GET)', function() {
        it('should return an empty response', function(){
            return r.get(urls.heartbeat).then(function(response) {
                expect({}).to.deep.equal({})
            }).catch(helper.skipIfOffline)
        })
    })
    
    after(function() {
        return helper.cleanDB()
    })
})
