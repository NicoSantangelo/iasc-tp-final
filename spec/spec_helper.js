var chai = require("chai")
var mongoose = require('mongoose')

var Bidding  = require('../models/bidding')
var Buyer    = require('../models/buyer')
var BidPrice = require('../models/bidPrice')

before("Connect to MongoDB", function() {
    mongoose.connect('mongodb://localhost/iac_test')
})

var cloneObject = function(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj
    }
 
    var temp = obj.constructor()
    for (var key in obj) {
        temp[key] = cloneObject(obj[key])
    }
 
    return temp
}

module.exports = {
    errorResponse: {
        name: 'RequestError',
        cause: {
            code: 'ECONNREFUSED'
        }
    },

    skipIfOffline: function(err) {
        if (err.cause && err.cause.code === 'ECONNREFUSED') {
            console.log('The server is OFFLINE, please start it with TEST=true as an env variable. Skipping this test')
        } else {
            throw new Error(err.message); 
        }
    },

    createBuyers: function() {
        return Promise.all([
            new Buyer({ name: 'Nicolas' }).save(),
            new Buyer({ name: 'Cristian' }).save(),
            new Buyer({ name: 'Axel' }).save()
        ])
    },
    
    cleanDB:function() {
        return Promise.all([
            Buyer.remove({}),
            Bidding.remove({}),
            BidPrice.remove({})
        ])
    },

    clone: cloneObject
}
