var chai = require("chai")
var expect = chai.expect

var r    = require('../request_wrapper')
var urls = require('../domain_urls')({ testing: true })

var helper = require('./spec_helper')

describe('The request wrapper', function() {
    describe('#heartbeat', function() {
        var timeBetweenBeats = 50
        it('should send a beat to the server every X number of milliseconds', function(done) {
            var counter = 0

            r = helper.clone(r)
            r.get = function(uri, params) {
                if (uri.search('heartbeat') !== -1) {
                    counter += 1
                }

                return new Promise(function(resolve) {
                    resolve('ok')
                })
            }

            r.heartbeat(urls.heartbeat, timeBetweenBeats).catch(function() {})

            setTimeout(function() {
                expect(counter).to.be.at.above(3)
                done()
            }, timeBetweenBeats * 5)
        })

        it('should return a promise that throws when the server is offline', function(done) {
            var counter = 0

            r = helper.clone(r)
            r.get = function(uri, params) {
                if (uri.search('heartbeat') !== -1) {
                    counter += 1
                }

                return new Promise(function(resolve, reject) {
                    if(counter < 3) {
                        resolve('ok')
                    } else {
                        reject(helper.errorResponse)
                    }
                })
            }

            r.heartbeat(urls.heartbeat, timeBetweenBeats).catch(function(error) {
                expect(error).to.be.equal('Connection refused')
                done()
            })
        })
    })

    describe('#isConnectionRefused', function() {
        it('should return true if the error catched passed is caused by a CONNREFUSED', function() {
            expect(r.isConnectionRefused(helper.errorResponse)).to.be.true
        })

        it('should return false otherwise', function() {
            expect(r.isConnectionRefused('nonsense')).to.not.be.ok
        })
    })
})
