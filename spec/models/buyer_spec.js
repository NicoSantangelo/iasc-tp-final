var chai = require("chai")
var chaiAsPromised = require("chai-as-promised")
var expect   = chai.expect

var Promise = require('bluebird')
var Buyer = require('../../models/buyer')

var helper = require('../spec_helper')

chai.use(chaiAsPromised)

describe('The Buyer model', function() {
  var aBuyer
  before(function() {
    return helper.createBuyers().then(function() {
      return Buyer.findOne({ name: 'Nicolas' })
    }).then(function(_buyer) {
      aBuyer = _buyer
    })
  })

  describe('.allExcept', function() {
    it('should return all buyers filtering by _id', function() {
      var props = { '_id': 0, 'name': 1 } 
      return expect(Buyer.allExcept(aBuyer, props).lean()).to.eventually.deep.include.members([{ name: 'Cristian' }, { name: 'Axel' }])
    })
  })

  after(function() {
    return helper.cleanDB()
  })

})
