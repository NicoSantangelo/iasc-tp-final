var chai = require("chai")
var chaiAsPromised = require("chai-as-promised")
var expect = chai.expect

var Promise = require('bluebird')
var Buyer   = require('../../models/buyer')
var Bidding = require('../../models/bidding')

var helper = require('../spec_helper')

chai.use(chaiAsPromised)

describe('The Bidding model', function() {
  var aBidding
  var attrs

  beforeEach(function() {
    var endTime = new Date()
    endTime.setSeconds(endTime.getSeconds() + 20)
    attrs = {
      title: 'Test bidding',
      price: 42,

      endTime: endTime,
      time   : 20000,

      status: 'Open'
    }
    var biddingPromise = new Bidding(attrs).save().then(function(bidding) {
      aBidding = bidding
    })
    return Promise.all([ biddingPromise, helper.createBuyers ])
  })

  describe('.oneByTitle', function() {
    it('should return the first match by title', function() {
      return Bidding.oneByTitle(attrs.title, { _id: 1 }).then(function(bidding) {
        expect(bidding._id.toString()).to.equal(aBidding._id.toString())
      })
    })
  })

  describe('#cancel', function() {
    it('It should save the bidding status to "Cancelled"', function() {
      return aBidding.cancel().then(function() {
        var query = { _id: aBidding._id }
        var props = { _id: 0, status: 1 }
        expect(Bidding.findOne(query, props).lean()).to.eventually.deep.equal({ status: 'Cancelled' })
      })
    })
  })

  describe('#findLosers', function() {
    it('It should return all buyers not matching the one stored on the bidding', function() {
      return Buyer.findOne({ name: 'Nicolas' }).then(function(winner) {
        var props = { _id: 0, name: 1 }
        aBidding.buyer = winner
        expect(aBidding.findLosers()).to.eventually.deep.include.members([
          { name: 'Cristian' },
          { name: 'Axel' }
        ])
      })
    })
  })

  afterEach(function() {
    return helper.cleanDB()
  })

})
