var chai = require("chai")
var expect   = chai.expect

var domainUrls = require('../domain_urls')

describe('The domain_urls module', function() {
  var urls = domainUrls()

  describe('#reconnect', function() {
    it('should increment the port size by one', function() {
      var urls = domainUrls({ testing: true })
      urls.reconnect()
      expect(urls.port).to.equal(8889)
    })

    it('should restart after a cap of X ports', function() {
      var urls = domainUrls({ testing: true, cap: 2 })
      urls.reconnect()
      urls.reconnect()
      urls.reconnect()
      expect(urls.port).to.equal(8888)
    })

    it('should return the new port', function() {
      var urls = domainUrls({ testing: true, cap: 2 })
      expect(urls.reconnect()).to.equal(8889)
    })
  })

  describe('the URLS', function() {
    describe('#port', function() {
      it('should be 3000 if in production', function() {
        expect(urls.port).to.equal(3000)
      })

      it('should be 8888 if in testing', function() {
        var testingUrls = domainUrls({ testing: true })
        expect(testingUrls.port).to.equal(8888)
      })

      it('should be configurable by the options argument', function() {
        var urls = domainUrls({ port: '1234' })
        expect(urls.port).to.equal(1234)
      })
    })

    describe('#base', function() {
      it('should set the server base url in production', function() {
        expect(urls.base).to.equal("http://127.0.0.1:3000")
      })

      it('should set the server base url in testing if testing is supplied', function() {
        var testingUrls = domainUrls({ testing: true })
        expect(testingUrls.base).to.equal("http://127.0.0.1:8888")
      })

      it('should be configurable by the options argument', function() {
        var baseUrl = 'http://some-base-url.com'
        var urls = domainUrls({ baseUrl: baseUrl })
        expect(urls.base).to.equal('http://some-base-url.com:3000')
      })
    })

    it('should set the buyers url using the base url', function() {
      expect(urls.buyers).to.equal("http://127.0.0.1:3000/buyers")
    })

    it('should set the buyers url using the base url', function() {
      expect(urls.bids).to.equal('http://127.0.0.1:3000/bids')
    })

    it('should set the sellers url using the base url', function() {
      expect(urls.sellers).to.equal('http://127.0.0.1:3000/sellers')
    })

    it('should set the heartbeat url using the base url', function() {
      expect(urls.heartbeat).to.equal('http://127.0.0.1:3000/heartbeat')
    })
  })

  describe('the Actions', function() {
     it('should set the buyers action using the base url', function() {
      expect(urls.actions.buyers).to.equal('/buyers')
    })

    it('should set the buyers action using the base url', function() {
      expect(urls.actions.bids).to.equal('/bids')
    })

    it('should set the sellers action using the base url', function() {
      expect(urls.actions.sellers).to.equal('/sellers')
    })

    it('should set the heartbeat action using the base url', function() {
      expect(urls.actions.heartbeat).to.equal('/heartbeat')
    })
  })
})

