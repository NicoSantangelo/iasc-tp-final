var chai = require("chai")

var expect   = chai.expect
var builder  = require('../builder')

describe('The builder module', function() {

  describe('.person', function() {
    it("should return a valid person from the list of names", function() {
      // Remove randomness
      builder.names = ['mockedName']

      expect(builder.person()).to.deep.equal({
        name: 'mockedName',
        contact: 'mockedname@gmail.com'
      })
    })
  })

})

